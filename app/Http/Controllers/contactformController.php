<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Mail;
class contactformController extends Controller
{
    public function formValidation(Request $request)
    {
		$validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
			'g-recaptcha-response' => 'required|captcha',
        ]);
		if ($validator->fails()) {
		return response()->json([
                'data' => $validator->errors()->all(),
            ]);
		}else{
			Mail::send('emails.contact', $request->all(), function($message) {
			 $message->to('jbora201@gmail.com', 'Contact Request')->subject('New Message from Frezit.com');
			 $message->from('info@frezit.com','Frezit');
		  });
		  return response()->json([
                'data' => 'success',
            ]);
		}
	}
}
