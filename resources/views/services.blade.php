@extends('layouts.layout')

@section('extracomponents')

<style>
.wrapper{
	padding:0;
}
.scroll{
	text-align:center;
	    float: left;
    width: 100%;
    padding: 120px 0;
	position:relative;
	background-size:cover;
	background-position:center;
}
.scroll:after{
	content: "";
    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    background: rgba(15,52,107,.8);
}
.scroll .col-md-6{
	position:relative;
	z-index:1;
}
.lead {
    font-size: 27px;
}
</style>
<script src="https://cdn.jsdelivr.net/npm/jquery.stellar@0.6.2/jquery.stellar.js"></script>
<script>
jQuery(document).ready(function(){
	jQuery(window).stellar();
})
</script>
@endsection

@section('content')

<div class="wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="scroll" style="background-image:url({{ asset('images/TEMPLATE-DESIGN.jpg') }})"  data-stellar-background-ratio="0.5">
				<div class="col-md-6  col-md-offset-3">
					<h2><span class="blue">Free</span> Demo</h2>
					<p class="lead">We are ready to show you a free demo of your site and please order your site only if we can make you happy.</p>
				</div>
			</div>
			<div class="scroll" style="background-image:url({{ asset('images/wordpress-developement.jpg') }})"  data-stellar-background-ratio="0.5" style="">
				<div class="col-md-6  col-md-offset-3">
					<h2><span class="blue">Wordpress</span> Developement</h2>
					<p class="lead">We are working on wordpress sites from last 8 years and have highly experienced developers who can bring your dream website into reality</p>
				</div>
			</div>
			<div class="scroll" style="background-image:url({{ asset('images/UXUI-DESIGN.jpg') }})"  data-stellar-background-ratio="0.5">
				<div class="col-md-6  col-md-offset-3">
					<h2><span class="blue">UX/UI</span> Design</h2>
					<p class="lead">We are best in modern technologies to build a frontend of a website. We can gurantee, you will not stop starring your website once its live.</p>
				</div>
			</div>
			<div class="scroll" style="background-image:url({{ asset('images/free-demo.jpg') }})"  data-stellar-background-ratio="0.5">
				<div class="col-md-6  col-md-offset-3">
					<h2><span class="blue">Template</span> Design</h2>
					<p class="lead">We have best designers in the market. Just say us what you want and what you like and our designer will send you samples in psd,jpg,pdf formats.</p>
				</div>
			</div>
			<div class="scroll" style="background-image:url({{ asset('images/image.jpg') }})"  data-stellar-background-ratio="0.5">
				<div class="col-md-6  col-md-offset-3">
					<h2><span class="blue">Logo</span> Design</h2>
					<p class="lead">We have creative designers who can design best and unique logos for you according to your business logic.</p>
				</div>
			</div>
			<div class="scroll" style="background-image:url({{ asset('images/ticket-system.jpg') }})"  data-stellar-background-ratio="0.5">
				<div class="col-md-6  col-md-offset-3">
					<h2><span class="blue">Ticket</span> System</h2>
					<p class="lead">We have a ticket system where we are bound to solve your issues and queries within 24hour.</p>
				</div>
			</div>
			<div class="scroll" style="background-image:url({{ asset('images/web-content.jpg') }})"  data-stellar-background-ratio="0.5">
				<div class="col-md-6  col-md-offset-3">
					<h2><span class="blue">Special</span> web content</h2>
					<p class="lead">We can consult with you regarding content requiring extra design work, information pulled from PeopleSoft, or other such projects.</p>
				</div>
			</div>
			<div class="scroll" style="background-image:url({{ asset('images/hosting.jpg') }})"  data-stellar-background-ratio="0.5">
				<div class="col-md-6  col-md-offset-3">
					<h2><span class="blue">Hosting</span></h2>
					<p class="lead">We host our sites in high level hosting servers which keep your site secure and maintenance free.</p>
				</div>
			</div>
			<div class="scroll" style="background-image:url({{ asset('images/seo.jpg') }})"  data-stellar-background-ratio="0.5">
				<div class="col-md-6  col-md-offset-3">
					<h2><span class="blue">Free Seo </span>(Search Engine Optimization)</h2>
					<p class="lead">When you are building a site with us we provide free seo analysis which will help your site to stay in first page of search engine results.</p>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection