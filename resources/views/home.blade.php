@extends('layouts.layout')

@section('extracomponents')

	<script src='https://www.google.com/recaptcha/api.js'></script>



@endsection

@section('content')

<!-- Header --> 
<header>
 <div class="intro">
	<!-- Site Logo -->
	<div class="logo"><img src="{{ asset('images/mainlogo.png') }}" alt="logo" /></div>
	<!-- End Site Logo -->
	
	<!-- Rotator Title --> 
	<div id="rotate">
	   <h1 class="rotate">We are <span>Twist</span><br><span>Great</span> Digital <span>Agency</span></h1>
	   <h1 class="rotate">We <span>Creative</span>. We <span>Think</span><br>We <span>Design</span>. your Dream</h1>
	</div>
	<!-- End Rotator Title -->
	
	<p>Creativity is Magical</p>
	<p><span></span><i class="fa fa-heart"></i><span></span></p>
	<p>Not Magic</p>
	
 </div>
 
 <!-- Nav -->
 <nav>
	<ul class="navigation">
	   <li class="link left middle side move"><a href="#about" title="Services">Career</a></li>
	   <li class="link right middle side move"><a href="#contact" title="Contact">Contact</a></li>
	</ul>
 </nav>
 <!-- End Nav -->
</header>
<!-- End Header -->

<!-- About Section -->
<section id="about" data-direction="from-left">
<div class="map spacing" style="background:url({{ asset('images/career.jpg') }}) no-repeat center;">
<div class="spacing" style="z-index: 99;">
 <h1>
	   <span>Work with us</span>
	</h1>
	</div> 
</div>
 <div class="container">
	<a href="#" class="close"></a>
	
	<!-- ./End Section Title -->
	<div class="row text-center">
	   <div class="col-sm-10 col-sm-offset-1" style="margin-top:20px;">
				<div class="text-center">
				We always keep searching for Rockstars of Web developement so we always have a vacancy for the right one :)<br /> 
				We are working on this page to display our all job posts together so that it become easy for you to contact us and join us.<br /><br />
				Till then please write to us at <strong>career@frezit.com</strong> or <strong>jbora201@gmail.com</strong> and we will contact you at the earliest.
				</div>
			</div>
	</div>
 </div>
</section>
<!-- End About Section -->

<!-- Contact Section -->
<section id="contact" data-direction="from-right">
<div class="map spacing" style="background:url({{ asset('images/map.png') }}) no-repeat center;">
<div class="spacing" style="z-index: 99;">
 <h1>
	   <span>Contact Us</span>
	</h1>
	</div> 
</div>
 <div class="container">  
	<!-- ./End Section Title -->
	<a href="#" class="close"></a>
	<div class="row">
	   <div class="col-md-8 col-md-offset-2" style="margin-top:20px;">
			<div id="message"></div>
			<form id="form" method="post" action="/contact">
			{{ csrf_field() }}
			  <div class="form-group col-sm-6">
				<label class="control-label" for="email">Name:</label>
				<input type="name" name="name" class="form-control" required>
			  </div>
			  <div class="form-group col-sm-6">
				<label class="control-label" for="pwd">Email:</label>
				<input type="email" name="email" class="form-control" required>
			  </div>
			  <div class="form-group col-sm-6">
				<label class="control-label" for="pwd">Subject:</label>
				<input type="text" name="subject" class="form-control">
			  </div>
			  <div class="form-group col-sm-6">
				<label class="control-label" for="pwd">Website:</label>
				<input type="text" name="website" class="form-control">
			  </div>
			  <div class="form-group col-sm-12">
				<label class="control-label" for="pwd">Message:</label>
				<textarea name="details" class="form-control" style="height: 100px;"></textarea>
			  </div>
			   <div class="form-group col-sm-12">
			  {!! app('captcha')->display() !!}
			   </div>
			  <div class="form-group col-sm-12"> 
				<button type="submit" class="btn btn-default">Submit</button>
			  </div>
			</form>
	   </div>
	</div>
 </div>
</section>
<!-- End Contact Section -->
<script>
$(document).ready(function(){
	if($(window).width()>768){
	var height=$(window).height()/3;
	$('.spacing').css('height',height+'px');
	}
	
	$("#form").submit(function (e) {
	e.preventDefault();  
	console.log($("#form").serialize());
	$.ajax({
		type : "POST",
		url : "/contact",
		dataType : "JSON",
		data:$("#form").serialize(),
		success : function(response) {
			if(response.data == 'success'){
				$('#message').html('<div class="alert alert-success"><strong>Success!</strong> we have received your message. One of our executive will contact you shortly.</div>');
				$("#form").trigger("reset");
			}else{
				var errors = '';
				for(var i=0;i<response.data.length;i++){
					errors += response.data[i]+'<br />';
				}
				$('#message').html('<div class="alert alert-danger">'+errors+'</div>');
			}
	  
			}
		}); 
	});
})
$(window).resize(function(){
	if($(window).width()>768){
	var height=$(window).height()/3;
	$('.spacing').css('height',height+'px');
	}
})
</script>
@endsection