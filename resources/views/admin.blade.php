@extends('layouts.layout')

@section('extracomponents')

<style>
.wrapper{
	margin-top:0;    clear: both;
}
</style>

@endsection

@section('content')
<div class="bar container-fluid">
<div class="row"><div class="col-sm-12">
<ul class="nav navbar-nav">
		<li><a href="/portfolios">Portfolios</a></li>
		<li><a href="/portfolio">Add Portfolio</a></li>
</ul>
<ul class="nav navbar-nav  navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
</div></div>
</div>
<div class="wrapper">
<div class="container-fluid">

</div>
</div>
@endsection