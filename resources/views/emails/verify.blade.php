@extends('layouts.layout')

@section('extracomponents')



@endsection

@section('content')

<div class="wrapper">
	<div class="container">
		<div class="row">
			<div class="col-sm-10 col-sm-offset-1">
				<h1>
				   <span>About Frezit</span>
				</h1>
				<p style="text-align: justify;"><span style="color: #48c0f4;"><strong>FREZIT</strong></span>&nbsp; was founded back in 2010. At present It is a leading website design company that blends technology quality and creativity together. Over the years we have set a benchmark in providing top quality service to our clients in the field of web Service. We offer a wide gamut of web related services like Web Design, Graphic Designing, e-commerce solutions, Web Hosting, Doman Registration, Bulk SMS, website maintenance for a wide array of sectors. So if you are considering outsourcing web designing or other web services with us, we promise to provide you good quality service that will be impeccable for your business as well as for your personal needs.</p>
				<p style="text-align: justify;">We have broadened our horizons by actively participating in the projects of our clients and finishing those according to their schedules. With a team of experts comprising of web developers and programmers and with the vast experience we have come about leaps and bounds in the web designing industry. The developers know to make use of the most recent breakthroughs in the field of web designing. <span style="color: #48c0f4;"><strong>FREZIT</strong></span> mainly renders its service globally in different countries like United States of America, United Kingdom, Australia, Saudi Arabia, Italy, Denmark etc.</p>
				<p style="text-align: justify;">Please contact us for consultation. Leave your phone number or Email id at our email account, so that we can get in touch with you. Just give your idea briefly that you want to implement and leave the rest on us. We will transform your idea into reality making it an online marketing marvel and wizard. We believe in investing time in providing dedicated services to the customers.</p>
			</div>
		</div>
	</div>
</div>

@endsection
